USER   = mel
HOST   = crafting.fr
NAME   = /usr/share/nginx/www/kisocsgames
BRANCH = master
REPO   = "$(USER)@$(HOST):$(NAME)"

deploy:
	@echo "Pushing branch \"$(BRANCH)\" to production"
	git push $(REPO) $(BRANCH)
	@echo "Deploy completed !"
