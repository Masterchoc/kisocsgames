//import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/index'
import Meta from 'vue-meta'
import './css/main.css'
Vue.use(Meta);

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

Vue.config.productionTip = false;
var whitelist = [
    '/',
    '/games',
    '/cart',
    '/support',
    '/activation',
    '/forgot-password',
    '/login',
    '/register'
];

router.beforeEach( (to, from, next) =>
{
    if(to.path.match(/activation/)) {
        next();
    }
    else if(to.path.match(/admin/) && store.getters.profile.role != 'admin')
    {
        next({path: '/'});
        return
    }
    else if(to.path.match(/games/))
        next();
    else if(to.path.match(/page/))
        next();
    else {
        if(whitelist.indexOf(to.path) == -1 && !store.getters.profile.token)
        {
            next({
                path: '/'
            });
        }
        else next()
    }
});
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');