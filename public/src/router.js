import Vue from 'vue'
import Router from 'vue-router'

import Home from './components/home/Home.vue'
import Game from './components/shop/Game.vue'
import GameListing from './components/shop/GameListing.vue'
import Support from './components/support/Support.vue'
import Account from './components/users/Account.vue'
import Activation from './components/users/Activation.vue'
import Dashboard from './components/users/Dashboard.vue'
import Servers from './components/users/Servers.vue'
import Bills from './components/users/Bills.vue'
import Cart from './components/shop/Cart.vue'
import News from './components/news/index.vue'
import Tickets from './components/users/Tickets.vue'

import AdminDashboard from './components/home/back/Home'
import AdminLocations from './components/locations/back/index'
import AdminProducts from './components/shop/back/products'
import AdminOrders from './components/orders/back/index'
import AdminUsers from './components/users/back/index'
import AdminTickets from './components/support/back/index'
import AdminNews from './components/news/back/index'

Vue.use(Router);

export default new Router({
    routes : [
        {path : '/', name : 'home', component : Home},
        {path : '/games', name : 'games', component : GameListing},
        {path : '/games/:id', name : 'game_view', component : Game},
        {path : '/support', name : 'support', component : Support},
        {path : '/account', name : 'account', component : Account},
        {path : '/activation/:email/:key', name : 'activation', component : Activation},
        {path : '/dashboard', name : 'dashboard', component : Dashboard},
        {path : '/servers', name : 'servers', component : Servers},
        {path : '/bills', name : 'bills', component : Bills},
        {path : '/cart', name : 'cart', component : Cart},
        {path : '/news', name : 'news', component : News},
        {path : '/tickets', name : 'tickets', component : Tickets},
        {path : '/admin-dashboard', name : 'admin_dashboard', component : AdminDashboard},
        {path : '/admin-products', name : 'admin_products', component : AdminProducts},
        {path : '/admin-locations', name : 'admin_locations', component : AdminLocations},
        {path : '/admin-orders', name : 'admin_orders', component : AdminOrders},
        {path : '/admin-users', name : 'admin_users', component : AdminUsers},
        {path : '/admin-tickets', name : 'admin_tickets', component : AdminTickets},
        {path : '/admin-news', name : 'admin_news', component : AdminNews}
    ]
})
