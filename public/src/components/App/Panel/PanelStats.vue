<template>
    <panel :toolbox="options.toolbox">
        <div slot="title">
            <div class="dropdown" style="float:right">
                <a
                    class="label pull-right dropdown-toggle"
                    :class="periods[selectedPeriod].class"
                    href="#"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false">
                    {{periods[selectedPeriod].name}} <i class="fa fa-chevron-down"></i>
                </a>
                <ul class="dropdown-menu" style="margin-left:4px;">
                    <li v-for="(period, index) in periods">
                        <a href="#" @click.prevent="selectedPeriod = index">{{period.name}}</a>
                    </li>
                </ul>
            </div>
            <h5 v-if="!options.linkedTitle">
                {{options.title}}
            </h5>
            <router-link
                :to="{name: options.titleUrl}"
                tag="h5"
                v-if="options.linkedTitle">
                <a>{{options.title}}</a>
            </router-link>
        </div>
        <div slot="content">
            <h1 class="no-margins">
                {{options.value.toString()}}&nbsp;
                {{options.foreignSymbol}}
            </h1>
            <div
                class="stat-percent font-bold"
                :style="evolutionColor"
                v-if="options.showEvolution && typeof options.evolution != 'undefined'">
                {{options.evolution.toFixed(1)}}%
                <i class="fa" :class="{
                    'fa-level-up': options.evolution > 0,
                    'fa-level-down': options.evolution <= 0
                }"></i>
            </div>
            <small>{{options.contentText}}</small>
        </div>
    </panel>
</template>
<script type="text/babel">
import Panel from './Panel.vue'
export default {
    data() {
        return {
            periods: [
                {name: 'Total', class: 'label-info'},
                {name: 'Today', class: 'label-primary'},
                {name: 'Monthly', class: 'label-success'},
            ],
            selectedPeriod: 2
        }
    },
    props: {
        options: {
            type: Object,
            default: {
                title: 'Header',
                label: 'Weekly',
                klass: 'success',
                titleUrl: 'home',
                linkedTitle: true,
                value: 0,
                showEvolution: false,
                foreignSymbol: '€',
                evolution: 0,
                toolbox: false,
                contentText: 'Content Text'
            }
        }
    },
    computed: {
        evolutionColor() {
            if(this.options.evolution < 5)
                return {color: '#ed5565'};
            else if(this.options.evolution > 5
                && this.options.evolution < 50)
                return {color: '#f8ac59'};
            else
                return {color: '#1ab394'};
        }
    },
    components: {
        Panel
    }
}
</script>