import Vue from 'vue'
import store from './LightboxStore'

Vue.directive('lightbox', {
    bind(el, binding) {
        console.log(binding)
        let index = store.addImage(el.getAttribute('href'));
        el.addEventListener('click', (e) => {
            e.preventDefault();
            store.open(index)
        })
    }
});
