import products from '../../../api/front/products'
import * as types from '../../mutations-types'


export default {
    state: {
        product: {},
        products: []
    },
    getters: {
        searchProduct: state => {
            return keyword => state.products.filter(product =>{
                return product.name.search(new RegExp(keyword, "i")) != -1;
            });
        },
        getProduct: state => state.product,
        getProducts: state => state.products,
        getRandomProducts: state => {
            let res = [];
            let ar = state.products;
            let idx = ar.findIndex(i => i.id == state.product.id);
            
            if(idx >= 0)
                ar.splice(idx, 1);
            
            const getRandomItem = (array) => {
                let item = array[~~(Math.random() * array.length)];
                let index = array.indexOf(item);
                array.splice(index, 1);
                console.log(item)
                return item;
            };
            
            for(let i = 0; i <= 3; i++) {
                res.push(getRandomItem(ar));
            }
            
            return res;
        }
    },
    actions: {
        setProduct(store, id) {
            products.get(id, res => {
                store.commit('SET_PRODUCT', res.data.data)
            })
        },
        setProducts(store, ctx) {
            products.findAll(res => {
                store.commit('SET_PRODUCTS', res.data.data)
            })
        }
    },
    mutations: {
        ['SET_PRODUCT'](state, product) {
            state.product = product;
        },
        ['SET_PRODUCTS'](state, products) {
            state.products = products;
        }
    }
}

