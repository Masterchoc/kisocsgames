import home from '../../../api/front/home'
import * as types from '../../mutations-types'

export default {
    state: {
        cities: []
    },
    getters: {
        getCities: state => state.cities
    },
    actions: {
        getPostalCode(store, ctx) {
            home.getPostalCode(ctx.register.postal_code, res => {
                if(res.data.success == true) {
                    if(res.data.data.length > 0) {
                        store.commit('RESET_CITIES');
                        res.data.data.forEach(city => {
                            store.commit('ADD_CITY', city.nomCommune)
                        });
                        ctx.register.city = res.data.data[0].nomCommune;
                    }
                }
            })
        }
    },
    mutations:
        {
            ['RESET_CITIES'](state) {
                state.cities = [];
            },
            ['ADD_CITY'](state, city) {
                state.cities.push(city);
            }
        }
}

