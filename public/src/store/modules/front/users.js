import users from '../../../api/front/users'
import api from '../../../api/api'
import * as types from '../../mutations-types'
import bus from '../../../bus'
import Vue from 'vue'

export default {
    state: {
        user: {},
    },
    getters: {
        profile: state => state.user,
    },
    actions: {
        login(store, ctx) {
            users.login(ctx.login, res => {
                if(res.data.code == 401) {
                    ctx.login.error = res.data.message;
                }
                else if(res.status == 200)
                {
                    ctx.updateDisplay();
                    let storage = ctx.login.remember === true ? window.localStorage : window.sessionStorage;
                    
                    storage.setItem('session_token', res.data.data.token);
                    storage.setItem('session_profile', JSON.stringify(res.data.data));
                    api.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.data.token;
    
                    localStorage.setItem('login_remember', ctx.login.remember);
                    store.commit(types.SET_USER, res.data.data);
                    ctx.$router.replace({name: 'dashboard'});
                    ctx.login.error = null;
                    ctx.login.email = '';
                    ctx.login.password = '';
                }
            })
        },
        
        register(store, ctx) {
            users.register(ctx.register, res => {
                if(res.data.code == 401) {
                    if(Array.isArray(res.data.errors)) {
                        if(res.data.errors.length > 0) {
                            for(let field in ctx.register.errors) {
                                ctx.register.errors[field].error    = false;
                                ctx.register.errors[field].messages = [];
                
                                for(let i = 0; i < res.data.errors.length; i++) {
                                    let error = res.data.errors[i];
                                    if(error.path == field) {
                                        ctx.register.errors[field].error = true;
                                        ctx.register.errors[field].messages.push(error.message);
                                    }
                                }
                            }
                        }
                    }
                    else {
                        ctx.register.error = res.data.message;
                    }
                }
                else if(res.data.code == 409) {
                    ctx.register.errors.email.error = true;
                    ctx.register.errors.email.messages = [res.data.message];
                }
                else if(res.status == 200) {
                    for(let field in ctx.register.errors) {
                        ctx.register.errors[field].error = false;
                        ctx.register.errors[field].messages = [];
                    }
                    ctx.register.success = true;
                }
            });
        },
        
        activation(store, payload) {
            users.activate(payload.data, res => {
                if(res.data.code == 401) {
                    payload.ctx.error = res.data.message;
                }
                else if(res.status == 200) {
                    payload.ctx.success = res.data.message;
                }
            })
        },
        
        logout(store) {
            users.logout(data => {
                store.commit(types.LOGOUT_USER);
                localStorage.setItem('login_remember', false);
            })
        },
        
        save(store, ctx) {
        
        }
    },
    mutations:
    {
        [types.SET_USER](state, user) {
            state.user = user;
        },
        [types.LOGOUT_USER](state) {
            state.user = {};
        }
    }
}

