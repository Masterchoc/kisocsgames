import orders from '../../../api/front/orders'
import * as types from '../../mutations-types'

export default {
    state: {
        cart: []
    },
    getters: {
        getCart: state => state.cart,
        getCartTotal: state => {
            let sum = 0;
            state.cart.forEach(item => {
                sum += (item.item.slot_price * item.slots) * item.item.period;
            });
            return sum.toFixed(2) + ' €'
        },
        getCartNbItems: state => {
            return state.cart.length;
        }
    },
    actions: {
        importCart(store) {
            let cart = localStorage.getItem('session_cart') != null ? localStorage.getItem('session_cart') : [];
            if(cart.length > 0)
                store.commit('SET_CART', JSON.parse(cart));
        },
        saveCart(store) {
            store.commit('SAVE_CART');
        },
        addToCart(store, product) {
            store.commit('ADD_PRODUCT', product);
            store.commit('SAVE_CART');
        },
        removeFromCart(store, id) {
            store.commit('REMOVE_PRODUCT', id);
            store.commit('SAVE_CART');
        },
        resetCart(store) {
            store.commit('RESET_CART');
        }
    },
    mutations:
    {
        ['SET_CART'](state, cart) {
            state.cart = cart;
        },
        ['SAVE_CART'](state) {
            localStorage.setItem('session_cart', JSON.stringify(state.cart));
        },
        ['ADD_PRODUCT'](state, product) {
            state.cart.push(product);
        },
        ['REMOVE_PRODUCT'](state, id) {
            state.cart = state.cart.filter((i) => i !== id);
        },
        ['RESET_CART'](state) {
            localStorage.removeItem('session_cart');
            state.cart = [];
        }
    }
}

