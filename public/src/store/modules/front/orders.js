import api from '../../../api/front/orders'
import * as types from '../../mutations-types'

export default {
    state: {
        orders_history: [],
        order_url: null,
        order_details: {}
    },
    getters: {
        orders_history: state => state.orders_history,
        order_details: state => state.order_details,
        order_url: state => state.order_url
    },
    actions: {
        setOrdersHistory(store, ctx) {
            api.getHistory(data => {
                ctx.loading = false;
                store.commit(types.SET_ORDERS_HISTORY, data.rows);
            })
        },
        placeOrder(store, data) {
            api.placeOrder(data, created => {
                if(typeof created.order_url !== 'undefined')
                    store.commit(types.SET_ORDER_URL, created.order_url);
            })
        },
        setOrderDetails(store, params)  {
            api.getDetails(params, data => {
                store.commit(types.SET_ORDER_RETURN_DETAILS, data);
            })
        }
    },
    mutations: {
        [types.SET_ORDER_RETURN_DETAILS](state, details) {
            state.order_details = details;
        },
        [types.SET_ORDERS_HISTORY](state, history) {
            state.orders_history = history;
        },
        [types.SET_ORDER_URL](state, url) {
            state.order_url = url;
        }
    }
}
