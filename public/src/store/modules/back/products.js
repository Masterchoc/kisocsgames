import products from '../../../api/back/products'
import Vue from 'vue'
import * as types from '../../mutations-types'

export default {
    state: {
        products: []
    },
    getters: {
        getAdminProducts: state => state.products
    },
    actions: {
        setAdminProducts(store, ctx) {
            products.findAll(res => {
                store.commit('SET_ADMIN_PRODUCTS', res.data.data)
            })
        },
        removeAdminProduct(store, id) {
            products.remove(id, res => {
                store.commit('REMOVE_ADMIN_PRODUCT', id);
            })
        },
        addAdminProduct(store, product) {
            products.add(product, res => {
                store.commit('ADD_ADMIN_PRODUCT', res.data.data);
            })
        },
        updateAdminProduct(store, data) {
            products.edit(data.id, data, res => {
                store.commit('UPDATE_ADMIN_PRODUCT', res.data.data);
            })
        }
    },
    mutations: {
        ['ADD_ADMIN_PRODUCT'](state, product) {
            state.products.push(product)
        },
        ['SET_ADMIN_PRODUCTS'](state, products) {
            state.products = products;
        },
        ['REMOVE_ADMIN_PRODUCT'](state, id) {
            state.products = state.products.filter(item => item.id != id);
        },
        ['UPDATE_ADMIN_PRODUCT'](state, data) {
            let index = state.products.findIndex(item => item.id === data.id);
            
            if(index >= 0) {
                let product = state.products[index];
                product = data;
                Vue.set(state.products, index, product);
            }
        }
    }
}

