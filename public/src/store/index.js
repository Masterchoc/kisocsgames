import Vue from 'vue'
import Vuex from 'vuex'

import Users from './modules/front/users'
import Home from './modules/front/home'
import Products from './modules/front/products'
import Cart from './modules/front/cart'
import Orders from './modules/front/orders'
import AdminProducts from './modules/back/products'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        Users,
        Home,
        Cart,
        Products,
        Orders,
        AdminProducts
    },
    strict: false
});