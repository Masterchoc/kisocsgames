import cfg from '../../../config/application'
import axios from 'axios'

const api = axios.create({
    baseURL: process.env.NODE_ENV == 'production'
        ? cfg.hostname
        : `http://${cfg.domain.dev}:${cfg.port}`
});

api.defaults.withCredentials = true;

export default api;