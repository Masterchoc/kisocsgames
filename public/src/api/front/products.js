import api from '../api'
let ENDPOINT = '/products';

export default
{
    findAll(cb) {
        api.get(ENDPOINT).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    get(id, cb) {
        api.get(ENDPOINT+'/'+id).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    }
}