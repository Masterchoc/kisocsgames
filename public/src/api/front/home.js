import api from '../api'

let ENDPOINT = '/postal_codes';
export default {
    getPostalCode(code, cb) {
        api.get(ENDPOINT + '/' + code).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
}