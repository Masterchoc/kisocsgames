import api from '../api'
let ENDPOINT = '/orders';

export default {
    placeOrder(order, cb) {
        api.post(ENDPOINT, order).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    getHistory(cb) {
        api.get(ENDPOINT + '/history').then(res => {
            if(res.status == 200)
                cb(res.data.data)
        }).catch(error => console.log(error));
    },
    
    getDetails(data, cb)
    {
        api.get(ENDPOINT + '/' + data.id + '?type='+data.type).then(res => {
            if(res.status == 200)
                cb(res.data.data)
        }).catch(error => console.log(error));
    }
}