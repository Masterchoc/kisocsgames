import api from '../api'
import * as types from '../../store/mutations-types';

let ENDPOINT = '/users/';
export default {
    login(credentials, cb) {
        api.post(ENDPOINT + 'auth', {email : credentials.email, password : credentials.password}).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    activate(data, cb) {
        api.get(ENDPOINT + 'activation/' + data.email + '/' + data.key).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    register(credentials, cb) {
        api.post(ENDPOINT + 'register', {
            email        : credentials.email,
            password     : credentials.password,
            token        : credentials.token,
            confirmation : credentials.confirmation
        }).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    logout(cb) {
        api.get(ENDPOINT + 'logout').then(res => {
            if(res.status === 200)
                cb(res.data.data)
        }).catch(error => console.log(error));
    },
    
    checkAuth(store, cb) {
        api.get(ENDPOINT + 'auth').then(res => {
            let storage = localStorage.getItem('login_remember') == 'true' ? 'localStorage' : 'sessionStorage';
            if(res.data.message === "Authentified") {
                let profile = JSON.parse(window[storage].getItem('session_profile'));
                api.defaults.headers.common['Authorization'] = 'Bearer ' + profile.token;
                store.commit(types.SET_USER, profile);
            }
            else {
                window[storage].removeItem('session_token');
                window[storage].removeItem('session_profile');
                window[storage].setItem('login_remember', false)
            }
            
            if(typeof cb == 'function')
                cb()
        }).catch(error => console.log(error));
    }
}