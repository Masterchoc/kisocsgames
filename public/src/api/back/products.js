import api from '../api'
let ENDPOINT = '/products';

export default
{
    findAll(cb) {
        api.get(ENDPOINT+'-all').then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    add(data, cb) {
        api.post(ENDPOINT, data).then(res => {
            cb(res);
        }).catch(error => console.log(error))
    },
    
    get(id, cb) {
        api.get(ENDPOINT+'/'+id).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    edit(id, data, cb) {
        api.put(ENDPOINT+'/'+id, data).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    },
    
    remove(id, cb) {
        api.delete(ENDPOINT+'/'+id).then(res => {
            cb(res);
        }).catch(error => console.log(error));
    }
}