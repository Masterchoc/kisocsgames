expressJwt   = require 'express-jwt'
pathToRegexp = require 'path-to-regexp'

module.exports = class Tokens
	constructor: (@app) ->
		@loaded = false
		@config =
			jwt:
				secret: @app.config.apis.jwt.key
				requestProperty: 'decoded'
			exclude:
				path: [
					'/'
					'/locales'
					'/support'
					'/products'
					'/servers'
					pathToRegexp '/products/:id'
					pathToRegexp  '/postal_codes/:code'
					'/users/auth'
					'/users/logout'
					'/users/register'
					'/users/recover'
					pathToRegexp '/users/activation/:email/:key'
					pathToRegexp '/.well_known/acme-challenge/'
				]

		@app.use(expressJwt(@config.jwt).unless(@config.exclude))

		@app.use (err, req, res, next) =>
			if err.name == 'UnauthorizedError'
				@app.error res, statusCode: 403, message: 'Invalid Access-Token'

		@app.logger.info '✓ '.bold.green+'Setup JSON Web Tokens.'
		@loaded = true


