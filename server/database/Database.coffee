Sequelize = require 'sequelize'

module.exports = class Database
	constructor: (@app) ->
		if process.env.NODE_ENV  == 'development'
			cfg = @app.config.database.dev
		else if process.env.NODE_ENV == 'production'
			cfg = @app.config.database.prod
		else if process.env.NODE_ENV == 'test'
			cfg = @app.config.database.dev
		else
			cfg = @app.config.database.dev

		if @app.logger
			@app.logger.info '✓ '.bold.green+'Selected new database : %s.',
				cfg.database

		return instance: new Sequelize(
			cfg.database,
			cfg.user,
			cfg.password,
				logging: false
				dialect: cfg.adapter
				timezone: "+01:00"
				dialectOptions:
					charset: 'utf8mb4'
				define:
					timestamps: true
					charset: 'utf8',
					collate: 'utf8_general_ci',

		),
		sequelize: Sequelize
