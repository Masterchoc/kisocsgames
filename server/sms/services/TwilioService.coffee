class TwilioService
	constructor: (cfg) ->
		@client = require('twilio')(cfg.twilio.key, cfg.twilio.token)

	send: (data, cb) ->
		@client.messages.create
			to    : data.to
			from  : data.from
			body  : data.body
		, (err, message) ->
				cb err, message

module.exports = TwilioService
