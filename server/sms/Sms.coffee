config = require '../../config/apis.json'

class Sms
	constructor: (name = 'Twilio') ->
		@name = name.toLowerCase()
		@service = @getService name
		return @

	getService: (name) ->
		service = require "./services/#{name}Service"
		return new service config

	send: (data, cb) ->
		if !data.from
			data.from = config[@name].from

		@service.send data, (err, message) ->
			cb err, message

module.exports = Sms

