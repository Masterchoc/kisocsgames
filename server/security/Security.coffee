pathToRegexp  = require 'path-to-regexp'
expressAcl    = require 'express-acl'

class Security
	constructor: (@app) ->
		@loaded = false
		@config =
			exclude:
				path: [
					'/'
					'/locales'
					'/support'
					'/products'
					'/servers'
					pathToRegexp '/products/:id'
					pathToRegexp '/locales/:code'
					pathToRegexp '/postal_codes/:code'
					pathToRegexp '/.well_known/acme-challenge/'
					pathToRegexp '/pages/:id'
					'/users/auth'
					'/users/logout'
					'/users/register'
					'/users/recover',
					pathToRegexp '/users/activation/:email/:key'
				]

		expressAcl.config
			baseUrl  : '/'
			filename : 'permissions.json'
			path     : 'config'

		@app.use expressAcl.authorize.unless @config.exclude

		@app.use (req, res, next) =>

			allowedOrigins = ['http://localhost:8080', 'http://zebooster.fr'];
			origin = req.headers.origin;

			if allowedOrigins.indexOf(origin) > -1
				res.setHeader 'Access-Control-Allow-Origin', origin

			res.setHeader 'Application-Name', @app.config.application.name
			res.header 'Access-Control-Allow-Headers', 'Cache-Control, Origin, X-Requested-With, Content-Type, Accept, Authorization'
			res.header 'Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS'
			res.header 'Access-Control-Allow-Credentials', true
			next()

		@app.logger.info '✓ '.bold.green+'Loaded users permissions.'
		@loaded = true

module.exports = Security
