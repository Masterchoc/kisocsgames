fs       = require 'fs'
_        = require 'lodash'
mustache = require 'mustache'
mailgun  = require 'mailgun-js'

class Mailer
	constructor: (@app, options = {}) ->
		@loaded   = false

		if process.env.NODE_ENV == 'production'
			@domain = @stripHttp @app.config.apis.mailgun.domain;
		else
			@domain = @stripHttp @app.config.apis.mailgun.sandboxDomain;

		@from     = options.from or "no-reply@#{@domain}"
		@ext      = options.ext  or 'html'
		@dir      = options.dir  or "#{__dirname}/../../layouts/emails/templates"
		@instance = mailgun apiKey: @app.config.apis.mailgun.key, domain: @domain
		@vars     =
			title: @app.config.application.name
			appName: @app.config.application.name
			year: new Date().getFullYear()
			domain:
				url: @domain
				name: @domain
			header: null
			content: null
			action:
				url: null
				text: null

		@app.logger.info '✓ '.bold.green+'Started mailer daemon.'
		@loaded = true

	stripHttp: (str) ->
		str.replace('https://', '').replace 'http://', ''

	loadTemplate: (file, cb) ->
		fs.readFile "#{@dir}/#{file}.#{@ext}", 'utf8', (err, data) ->
			cb data if err is null

	parse: (template, vars) ->
		return mustache.to_html template, vars

	loadPrefab: (prefab, options, cb) ->
		vars = require "#{__dirname}/../../layouts/emails/prefabs/#{prefab}.json"
		@loadTemplate vars.template, (html) =>
			parsed = @parse html, _.merge(_.merge(_.merge(@vars, vars), options), options.data)
			cb({
				from    : options.from or @from
				to      : options.to
				subject : options.subject or vars.subject
				html    : parsed
			})

	send: (prefab, options, cb) ->
		@loadPrefab prefab, options, (data) =>
			@instance.messages().send data, (err, body) =>
				console.log err
				@app.logger.info '[%s] Email sent via mailgun', prefab.toUpperCase()
				cb err, body if typeof cb == 'function'

module.exports = Mailer
