redis       = require 'redis'
session     = require 'express-session'
RedisStore  = require('connect-redis')(session)
MemoryStore = session.MemoryStore;

module.exports = class Session
	constructor: (@app) ->
		@loaded = false
		@config =
			secret : @app.config.apis.jwt.key
			cookie :
				maxAge: 7 * 24 * 60 * 60 * 1000
			name   : 'session'
			secure : true
			resave: true
			store: new MemoryStore()
			saveUninitialized: true

		if process.env.NODE_ENV == 'production'
			@config.store = new RedisStore
				host   : 'http://127.0.0.1'
				port   : 6379,
				client : redis.createClient()
				ttl    : 999999

		@app.use (req, res, next) =>
			if !req.decoded
				req.decoded = role: 'guest'
			next()

		@app.use session @config
		@app.logger.info '✓ '.bold.green + 'Start session middleware.'
		@loaded = true
