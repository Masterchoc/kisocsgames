config = require '../../config/apis.json'
paypal = require 'paypal-rest-sdk'

class Paypal
	constructor: ->
		paypal.configure config.paypal

	createPayment: (req, data) ->
		payment =
			intent: 'sale'
			payer: payment_method: data.type
			transactions: data.transactions

		if data.type == 'credit_card'
			payment.payer.funding_instruments = [credit_card: data.credit_card]

		else if data.type == 'paypal'
			payment.redirect_urls =
				return_url: data.return_url
				cancel_url: data.cancel_url

		return new Promise (res, rej) ->
			paypal.payment.create payment, (error, payment) ->
				if error
					console.log error
					rej error
				else
					if payment.payer.payment_method == 'paypal'
						req.session.paymentId = payment.id
						redirect_url = ''

						for link in payment.links
							if link.method == 'REDIRECT'
								redirect_url = link.href

						res
							url: redirect_url
							id: payment.id

	execute: (paymentId, payerId) ->
		return new Promise (res, rej) ->
			paypal.payment.execute paymentId, payer_id: payerId, (error, payment) ->
				if error
					rej error.response.message
				else
					res payment


module.exports = Paypal

