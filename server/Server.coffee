http    = require 'http'
colors  = require 'colors'
express = require 'express'
events  = require 'events'

Logger   = require './logger/Logger'
Mailer   = require './mailer/Mailer'
Engine   = require './rendering/Engine'
Request  = require './requests/Request'
Locales  = require './locales/Locales'
Session  = require './session/Session'
Sockets  = require './sockets/Sockets'
Tokens   = require './tokens/Tokens'
Security = require './security/Security'
Database = require './database/Database'
Api      = require './api/Api'

CFG_DIR = "#{__dirname}/../config"
Config =
	database    : require "#{CFG_DIR}/database"
	apis        : require "#{CFG_DIR}/apis"
	application : require "#{CFG_DIR}/application"
	clusters    : require "#{CFG_DIR}/clusters"
	mailer      : require "#{CFG_DIR}/mailer"
	permissions : require "#{CFG_DIR}/permissions"

module.exports = class Server
	constructor : (options = {}) ->
		@port       = process.env.PORT or Config.application.port
		@app        = express()
		@app.config = Config
		@app.logger = Logger

	start : (cb) ->
		@app.logger.info '\u279F'.red.bold + ' Starting server...'

		@engine   = new Engine @app
		@request  = new Request @app
		@session  = new Session @app
		@locales  = new Locales @app
		@tokens   = new Tokens @app
		@security = new Security @app
		@mailer   = new Mailer @app

		@instance = http.createServer @app
		@api      = new Api @app, (models) =>
			@listen cb
		@sockets  = new Sockets @app, @instance

		@app.mailer = @mailer;
		@app.events = new events.EventEmitter()

		@app.events.on 'mail:send', (payload) =>
			@mailer.send payload.template,
				to : payload.to,
				data: payload.data
			(err, body) =>
				console.log err, body

		@app.events.on 'sockets:all', (payload) =>
			console.log payload

	listen : (cb) ->
		if process.env.NODE_ENV == 'development'
			domain = @app.config.application.domain.dev
		else if process.env.NODE_ENV == 'production'
			domain = @app.config.application.domain.prod

		@instance.listen @port, =>
			char = '*'
			str = 'Server started on port: %s'

			@app.logger.info char.repeat(str.length + @port.toString().length + 2).cyan
			@app.logger.info char.cyan + ' '.repeat(str.length + @port.toString().length) + char.cyan
			@app.logger.info char.cyan + ' ' + str + ' ' + char.cyan, @port.toString().bold.green
			@app.logger.info char.cyan + ' '.repeat(str.length + @port.toString().length) + char.cyan
			@app.logger.info char.repeat(str.length + @port.toString().length + 2).cyan

			cb() if typeof cb is 'function'
