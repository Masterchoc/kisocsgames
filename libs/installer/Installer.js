const path = require('path');
const glob = require('glob');

module.exports = class Installer
{
    constructor() {
        this.recipes = {};
        this.loadRecipes(path.join(process.cwd(), 'libs/installer/recipes'));
    }
    
    loadRecipes(folder) {
        glob(folder + '/**/*.js', (err, files) => {
            files.forEach(file => {
                let recipe = require(file);
                let filename = path.basename(file);
                let name = filename.substring(0, filename.indexOf('.'));
                this.recipes[name] = new recipe();
            })
        });
    }
    
    install(name, config = {}) {
        return new Promise((res, rej) => {
            if(typeof this.recipes[name] == 'undefined')
                rej('Recipe not found');
 
            res(this.recipes[name].install(config));
        })
    }
};
