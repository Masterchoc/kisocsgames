const Recipe = require('../Recipe');

module.exports = class CsgoRecipe extends Recipe {
    constructor() {
        super();
        this.dependencies = [
            'mailutils', 'postfix', 'curl', 'wget', 'file', 'bzip2',
            'gzip', 'unzip', 'bsdmainutils', 'python', 'util-linux',
            'ca-certificates', 'binutils', 'bc', 'jq', 'tmux',
            'lib32gcc1', 'libstdc++6', 'libstdc++6:i386'
        ];
        this.name         = 'csgoserver';
        this.modes        = {
            arms_race   : '1:0',
            casual      : '0:0',
            competitive : '0:1',
            custom      : '3:0',
            deathmatch  : '1:2',
            demolition  : '1:1'
        };
        this.user         = {
            name     : 'truc',
            group    : '12345',
            password : 'bidule'
        }
    }
    
    changeState(state) {
        this.run('connect', {
            host     : config.ip,
            username : this.user.name,
            password : this.user.password
        });
    
        this.run('startScript', {
            path  : '/home/' + this.user.name + '/' + this.name,
            args  : state
        });
    }
    
    install(config) {
        //this.run('connect', {
        //    host     : config.ip,
        //    username : 'root',
        //    password : config.password
        //});
        //this.run('updateAPT');
        //this.run('upgradeAPT');
        //this.run('addUser', {
        //    username : this.user.name,
        //    password : this.user.password,
        //    group    : this.user.group
        //});
        //this.run('mkdir', '/home/' + this.user.name);
        //this.run('installPackages', this.dependencies);
        //this.run('downloadFile', {
        //    src  : 'https://linuxgsm.sh',
        //    dest : '/home/truc/linuxgsm.sh'
        //});
        //this.run('chmod', {
        //    flags : '+x',
        //    path  : '/home/' + this.user.name + '/linuxgsm.sh'
        //});
        //this.run('chown', {
        //    flags : '-R',
        //    user  : this.user.name,
        //    group : this.user.group,
        //    path  : '/home/' + this.user.name
        //});
        //this.setShell('user');
        //this.run('connect', {
        //    host     : config.ip,
        //    username : this.user.name,
        //    password : this.user.password
        //});
        //this.run('startScript', {
        //    label : 'Creating "' + this.name + '" installer...',
        //    path  : '/home/' + this.user.name + '/linuxgsm.sh',
        //    args  : this.name
        //});
        //this.run('startScript', {
        //    label : 'Updating installer "' + this.name + '"...',
        //    path  : '/home/' + this.user.name + '/' + this.name,
        //    args  : 'install',
        //    cb    : chunk => {
        //        let expr = new RegExp('/home/' + this.user.name, 'ig');
        //
        //        if(chunk.toString('utf8').match(expr))
        //            this.getShell('user').dispose();
        //    }
        //});
        //this.run('connect', {
        //    host     : config.ip,
        //    username : this.user.name,
        //    password : this.user.password
        //});
        //this.run('sed', {
        //    flags : '-i',
        //    args  : '\'20,24d\'',
        //    input : '/home/' + this.user.name + '/lgsm/functions/install_server_dir.sh'
        //});
        //this.run('startScript',
        //{
        //    label : 'Installing "' + this.name + '"...',
        //    path  : '/home/' + this.user.name + '/' + this.name,
        //    args  : 'install',
        //    cb    : chunk => {
        //        //console.log('[STDOUT]', chunk.toString('utf8'));
        //        let percent = null;
        //        let lines = chunk.toString('utf8').split('\n');
        //
        //        for(let line of lines) {
        //            let progress = line.replace(/(.*)progress:\s([0-9.]+)(.*)/gi,
        //                (match, contents, offset) => offset);
        //
        //            if(!isNaN(parseInt(progress)))
        //                percent = parseInt(progress);
        //        }
        //
        //        if(percent)
        //            this.emit('progress', {value: percent});
        //
        //        if(chunk.toString('utf8').match(new RegExp('fully installed', 'gi'))) {
        //            this.emit('progress', {value: 100});
        //            this.getShell('user').dispose();
        //        }
        //    }
        //});
        //
        
        this.run('parseTemplate', {
            destination : '/home/' + this.user.name + '/lgsm/config-lgsm/' + this.name + '/' + this.name + '.cfg',
            template    : 'csgo',
            options     : {
                type       : this.modes[config.mode][0] || '0',
                mode       : this.modes[config.mode][1] || '1',
                mapgroup   : config.mapgroup || 'mg_active',
                defaultmap : config.defaultmap || 'de_mirage',
                maxplayers : config.maxplayers || '16',
                tickrate   : config.tickrate || '64',
                gslt       : config.gslt || ''
            }
        });
        
        this.changeState('start');
        this.startInstall();
        
        return this;
    }
};