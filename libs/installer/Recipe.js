const events        = require('events');
const ssh           = require('node-ssh');
const path          = require('path');
const fs            = require('fs');
const mustache      = require('mustache');

const promiseSerial = funcs =>
    funcs.reduce((promise, func) =>
            promise.then(result => func().then(Array.prototype.concat.bind(result))),
        Promise.resolve([]));

module.exports = class Recipe extends events.EventEmitter {
    constructor() {
        super();
        this.current_shell = 'root';
        this.cmds          = [];
        this.shells        = {
            root : new ssh(),
            user : new ssh()
        }
    }
    
    setShell(name) { this.shell = name; }
    
    getShell() { return this.shells[this.current_shell]; }
    
    parseTemplate(config) {
        return new Promise((res, rej) => {
            let file = path.join(process.cwd(), '/libs/installer/templates/'+config.template+'.tpl');
            fs.readFile(file, {encoding: 'utf8'}, (err, data) => {
                if(err) rej(err);
                else {
                    mustache.parse(data, ['<%', '%>']);
                    let str = mustache.render(data, config.options);
                    this.execCmd('Copying template config...', `echo "${str}" > ${config.destination}`).then(copied => {
                        res(copied)
                    })
                }
            })
        })
    }
    
    connect(options) {
        return new Promise((res, rej) => {
            this.getShell().connect(options)
            .then(data => {
                console.log(`Connected to "${options.host}" as "${options.username}"`);
                res(data)
            })
            .catch(error => rej(error))
        })
    }
    
    addUser(options) {
        return this.execCmd('Added user: "' + options.username + '"', `useradd -u ${options.group} -g users -d /home/${options.username} -s /bin/bash -p $(echo "${options.password}" | openssl passwd -1 -stdin) ${options.username}`);
    }
    
    startScript(options = {}) { return this.exec(options.label ? options.label : 'Started script: "' + options.path + '"', `bash ${options.path} ${options.args}`, options.cb); }
    
    updateAPT(options = {}) { return this.execCmd('Updating APT...', 'apt-get update'); }
    
    upgradeAPT(options = {}) { return this.execCmd('Upgrading APT...', 'apt-get upgrade -y'); }
    
    installPackages(packages = {}) { return this.execCmd('Installing packages...', 'apt-get install -y ' + packages.join(' ')); }
    
    downloadFile(options) { return this.execCmd('Downloading file: "' + options.src + '"', `wget -O ${options.dest} ${options.src}`); }
    
    mkdir(path) { return this.execCmd('Created folder: "' + path + '"', 'mkdir ' + path); }
    
    chmod(options) { return this.execCmd('Changing file permissions of: "' + options.path + '"', `chmod ${options.flags} ${options.path}`); }
    
    chown(options) { return this.execCmd('Changing owner of: "' + options.path + '"', `chown ${options.flags} ${options.user}:${options.group} ${options.path}`); }
    
    sed(options) { return this.execCmd('Sed file: "' + options.input + '"', `sed ${options.flags} ${options.args} ${options.input}`)}
    
    execCmd(label, cmd) {
        return new Promise((res, rej) => {
            this.getShell().execCommand(cmd)
            .then(data => {
                console.log(label);
                res(data.stdout)
            })
            .catch(error => res(error))
        })
    }
    
    exec(label, cmd, cb) {
        let onStdout = () => {};
        if(typeof cb != 'undefined')
            onStdout = cb;
        
        return new Promise((res, rej) => {
            this.getShell()[(typeof cb == 'undefined' ? 'execCommand' : 'exec')](cmd, [], {
                onStdout : onStdout
            })
            .then(data => {
                console.log(label);
                res(data.stdout)
            })
            .catch(error => res(error))
        })
    }
    
    run(name, args = {}) {
        if(typeof this[name] == 'function') {
            this.cmds.push({cmd : name, args : args});
        }
    }
    
    startInstall() {
        const funcs = this.cmds.map(payload => () => this[payload.cmd](payload.args));
        promiseSerial(funcs).then(() => {
        
        })
        .catch(e => console.log(e.message))
    }
};