/**
 * Created by Mel on 01/08/2016.
 */
const codesPostaux = require('codes-postaux');

module.exports = function(app, Sequelize) {
    return controller = {
        index        : function(req, res) {
            res.render('home/views/index.html', {
                page_name : app.config.application.name
            });
        },
        postal_codes: function(req, res) {
            let results = codesPostaux.find(req.params.code);
            if(typeof results[0] == 'undefined') {
                app.error(res, {message: 'Not found'});
            } else {
                app.success(res, results);
            }
        }
    }
};
