module.exports = function(app)
{
    var ctrl = app.api.controllers.servers;
    app.get('/servers', ctrl.index);
};
