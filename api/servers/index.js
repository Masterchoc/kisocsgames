const Vultr    = require('vultr');
const fs       = require('fs');
const path     = require('path');
const node_ssh = require('node-ssh');

const Installer = require('../../libs/installer/Installer');
let installer = new Installer();

let toSecs = time => (time[0] + (time[1] / 1e9)).toFixed(3);
let pad = n => n <= 9 ? '0' + n : n;

module.exports = function(app, Sequelize) {
    var vultrInstance = new Vultr(app.config.apis.vultr.key);
    return controller = {
        index : function(req, res) {
            vultrInstance.server.list().then(list => {
                let server = list[17788153];
                let start = process.hrtime();
                
                installer.install('csgo', {
                    ip: server.main_ip,
                    password: server.default_password,
                    gslt: app.config.apis.gslt.key,
                    mode: 'arms_race'
                }).then(install =>
                {
                    install.on('progress', data => {
                        console.log('progress', data.value + ' %');
                        if(data.value == 100) {
                            let end = toSecs(process.hrtime(start));
                            let s = ~~(end % 60);
                            let m = ~~(end / 60);
                            let h = ~~(m / 60);
                            
                            console.log('Installation finished in: '+pad(h) +':'+pad(m)+':'+pad(s));
                        }
                    });
                    install.on('end', () => {
                        console.log('install finished');
                    })
                }).catch(error => console.log(error))
            });
            
            // DCID: 24 (paris)
            // Plan: 201 (VPS 5$ - 1go)
            // OsID: 244 (debian 9 - x64)
            
            //vultrInstance.server.create({
            //    'plan': 201,
            //    'os': 244,
            //    'region': 24
            //}).then(response => {
            //   res.json(response)
            //})
            //vultrInstance.server.list().then(function(list) {
            //    let server        = list[17788153];
            //    let ssh           = new node_ssh();
            //    let sshUser       = new node_ssh();
            //    let game_dir_name = 'csgoserver';
            //    let user_password = 'password';
            //    let shells = {user: sshUser, root: ssh};
            //
            //    let dependencies = [
            //        'mailutils','postfix','curl','wget','file','bzip2',
            //        'gzip','unzip','bsdmainutils','python','util-linux',
            //        'ca-certificates','binutils','bc','jq','tmux',
            //        'lib32gcc1','libstdc++6','libstdc++6:i386'
            //    ];
            //
            //    const promiseSerial = funcs =>
            //        funcs.reduce((promise, func) =>
            //                promise.then(result => func().then(Array.prototype.concat.bind(result))),
            //            Promise.resolve([]));
            //
            //    let cmds = [
            //        {shell: 'root', name: 'Open new ssh session with user: root', program: 'connect', cmd: {host: server.main_ip, username : 'root', password : server.default_password}},
            //        {shell: 'root', name: 'Add user: '+game_dir_name, cmd: 'useradd -u 12345 -g users -d /home/' + game_dir_name + ' -s /bin/bash -p $(echo ' + user_password + ' | openssl passwd -1 -stdin) ' + game_dir_name},
            //        {shell: 'root', name: 'Create folder: /home/'+game_dir_name, cmd: 'mkdir /home/' + game_dir_name},
            //        {shell: 'root', name: 'Update system', cmd: 'apt-get update'},
            //        {shell: 'root', name: 'Upgrade system', cmd: 'apt-get upgrade -y'},
            //        {shell: 'root', name: 'Install dependencies', cmd: 'apt-get install -y ' + dependencies.join(' ')},
            //        {shell: 'root', name: 'Get game server manager', cmd: 'wget -O  /home/' + game_dir_name + '/linuxgsm.sh https://linuxgsm.sh'},
            //        {shell: 'root', name: 'Change permissions of the manager', cmd: 'chmod +x /home/' + game_dir_name + '/linuxgsm.sh'},
            //        {shell: 'root', name: 'Change owner of the manager', cmd: 'chown -R ' + game_dir_name + ':12345 /home/' + game_dir_name},
            //        {shell: 'user', name: 'Open new ssh session with user: '+game_dir_name, program: 'connect', cmd: {host: server.main_ip, username : game_dir_name, password : user_password}},
            //        {shell: 'user', name: 'Create '+game_dir_name+ ' installer', cmd: 'bash /home/' + game_dir_name + '/linuxgsm.sh csgoserver'},
            //        {shell: 'user', name: 'Install game server', program: 'exec', cmd: 'bash /home/' + game_dir_name + '/csgoserver install',
            //            onStdout: function(chunk)  {
            //                console.log('[STDOUT]', chunk.toString('utf8'));
            //                if(chunk.toString('utf8').match(new RegExp('/home/' + game_dir_name, 'ig'))) {
            //                    shells.user.dispose();
            //                    return;
            //                }
            //
            //        }},
            //        {shell: 'user', name: 'Reconnecting user: '+game_dir_name, program: 'connect', cmd: {host: server.main_ip, username : game_dir_name, password : user_password}},
            //        {shell: 'user', name: 'Sed installer to escape promppts', cmd: 'sed -i \'20,24d\' /home/' + game_dir_name + '/lgsm/functions/install_server_dir.sh'},
            //        {shell: 'user', name: 'Install game server', program: 'exec', cmd: 'bash /home/' + game_dir_name + '/csgoserver install',
            //            onStdout: function(chunk) {
            //                console.log('[STDOUT]', chunk.toString('utf8'));
            //                if(chunk.toString('utf8').match(new RegExp('fully installed', 'gi')))
            //                    console.log('[INSTALLATION FINISHED]');
            //        }}
            //    ];
            //
            //    let exec = (payload) => {
            //        return new Promise((resolve, reject) => {
            //           let program = 'execCommand';
            //            if(typeof payload.program != 'undefined')
            //                program = payload.program;
            //
            //            let onStdout = () => {};
            //            if(typeof payload.onStdout != 'undefined')
            //                onStdout = payload.onStdout;
            //
            //            if(program == 'connect') {
            //                shells[payload.shell].connect(payload.cmd).then(data => {
            //                    console.log(payload.name);
            //                    resolve(payload.name)
            //                }).catch(error => reject(error));
            //            }else {
            //                shells[payload.shell][program](payload.cmd, [], {onStdout: chunk => {
            //                        onStdout(chunk)
            //                    }}).then(data => {
            //                    console.log(payload.name);
            //                    resolve(payload.name)
            //                }).catch(error => resolve(error));
            //            }
            //        });
            //    };
            //
            //    const funcs = cmds.map(payload => () => exec(payload));
            //    promiseSerial(funcs).then(data => {})
            //    .catch(e => console.log(e.message))
            //})
            //
            //vultrInstance.regions.list().then(function(list) {
            //    res.json(list)
            //})
            
            //vultrInstance.os.list().then(function(list) {
            //    res.json(list)
            //})
            //vultrInstance.regions.availability(24).then(function(plans) {
            //   res.json(plans)
            //})
        }
    }
};
