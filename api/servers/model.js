const Vultr = require('vultr');

module.exports = (db, Sequelize, app) => {
    let vultrInstance = new Vultr(app.config.apis.vultr.key);
    let Server        = db.define('server', {});
    
    Server.getOsList = () => {
        return new Promise((res, rej) => {
            vultrInstance.os.list()
            .then(list => res(list))
            .catch(error => rej(error))
        })
    };
    
    Server.getRegionsList = () => {
        return new Promise((res, rej) => {
            vultrInstance.regions.list()
            .then(list => res(list))
            .catch(error => rej(error))
        })
    };
    
    Server.listRemoteServers = () => {
        return new Promise((res, rej) => {
            vultrInstance.server.list()
            .then(list => res(list))
            .catch(error => rej(error))
        })
    };
    
    Server.add = (plan, os, region) => {
        return new Promise((res, rej) => {
            vultrInstance.server.create({
                plan   : plan,
                os     : os,
                region : region
            })
            .then(data => res(data))
            .catch(error => rej(error))
        })
    };
    
    Server.isAvailable = (plan, region) => {
        return new Promise((res, rej) => {
            vultrInstance.regions.availability(region)
            .then(plans => res(plans.indexOf(plan) >= 1))
            .catch(error => rej(error))
        })
    };
    
    Server.getBandswitch = serverId => {
        return new Promise((res, rej) => {
            vultrInstance.server.bandwidth(serverId)
            .then(bandwidth => res(bandwidth))
            .catch(error => rej(error))
        })
    };
    
    Server.reboot = serverId => {
        return new Promise((res, rej) => {
            vultrInstance.server.reboot(serverId)
            .then(data => res(data))
            .catch(error => rej(error))
        })
    };
    
    Server.destroy = serverId => {
        return new Promise((res, rej) => {
            vultrInstance.server.destroy(serverId)
            .then(data => res(data))
            .catch(error => rej(error))
        })
    };
    
    Server.associate = models =>
    {
    
    };
    
    return Server;
};