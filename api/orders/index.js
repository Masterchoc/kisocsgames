var url      = require('url');
var qs       = require('querystring');
var luhn     = require('luhn-alg');
var Paypal   = require('../../server/payments/Paypal');
var paypal   = new Paypal();
var moment   = require('moment');
var cfg      = require('../../config/application');
var cardType = require('credit-card-type');

const PAYPAL_CARDS = {
    VISA             : 'visa',
    MASTERCARD       : 'mastercard',
    AMERICAN_EXPRESS : 'amex',
    DISCOVER         : 'discover',
    MAESTRO          : 'maestro'
};

module.exports = (app, Sequelize, db) =>
{
    let Orders =
    {
        index: (req, res) => {
            if(typeof req.session.user != 'undefined') {
                if(req.session.user.role == 'admin') {
                    return app.api.models.orders.findAndCountAll({
                        order : [['createdAt', 'ASC']]
                    })
                    .then(orders => app.success(res, orders))
                    .catch(error => app.error(res, error));
                }
            }
        },
        view: (req, res) => {
            if(typeof req.session.user != 'undefined'
            && typeof req.params.id != 'undefined'
            && typeof req.query.type != 'undefined') {
                if(req.query.type == 'location') {
                    return app.api.models.orders.find({
                        where: {
                            id: req.params.id
                        }
                    })
                    .then(order =>
                    {
                        console.log(order)

                        app.success(res, order);
                    })
                    .catch(error => console.log(error));


                }
            }
        },
        history: (req, res) => {
            if(typeof req.session.user != 'undefined') {
                return app.api.models.orders.findAndCountAll({
                    where: {userId: req.session.user.id},
                    order: [['updatedAt', 'DESC']]
                })
                .then(orders => {
                    app.success(res, orders);
                })
                .catch(error => {
                    console.log(error);
                })
            }
        },
        isCardValid: card =>
        {
            if(typeof card.name   == 'undefined'
            || typeof card.expiry == 'undefined'
            || typeof card.number == 'undefined'
            || typeof card.cvc    == 'undefined')
                return false;

            return luhn(card.number) == true;
        },
        trigger: (req, order, method, hours, room, transaction) =>
        {
            return new Promise((resolve, reject) =>
            {
                return app.api.models.room_locations.create(
                {
                    status   : 'order_waiting',
                    start    : null,
                    end      : null,
                    roomId   : room.id,
                    userId   : req.session.user.id,
                    duration : hours
                })
                .then(location_created =>
                {
                    console.log(qs.parse(url.parse(order.url).query).token);
                    return app.api.models.orders.create(
                    {
                        method         : method,
                        status         : 'waiting',
                        amount         : transaction.amount.total,
                        currency       : transaction.amount.currency,
                        description    : transaction.description,
                        token          : qs.parse(url.parse(order.url).query).token,
                        orderId        : order.id,
                        roomLocationId : location_created.id,
                        userId         : req.session.user.id,
                    })
                    .then(order_created =>
                    {
                        if(method == 'paypal')
                            resolve(order.url);
                        else if(method == 'credit_card')
                            resolve();
                    })
                    .catch(error => console.log(error));
                })
                .catch(error => console.log(error));
            });
        },
        isProductActive: (gameId) => {
            return new Promise((res, rej) => {
                app.api.models.products.find({
                    where: { active: true, id: gameId }
                }).then(data => {
                    res(data)
                }).catch(error => rej(error))
            })
        },
        create: (req, res) =>
        {
            if(typeof req.session.user  != 'undefined' && Array.isArray(req.body))
            {
                // Check game availability
                let promises = [];
                
                for(let i = 0; i < req.body.length; i++) {
                    promises.push(Orders.isProductActive(req.body[i].id))
                }

                Promise.all(promises).then(finished => {
                    console.log(finished)
                });
                
                //Orders.isProductActive(req.body.ref_id)
                //.then(room =>
                //{
                //    let errors = {}, hours;
                //    if(typeof req.body.ref_data.hours == 'undefined')
                //        error.hours = 'The hours requested is not valid';
                //    else {
                //        hours = parseInt(req.body.ref_data.hours);
                //        if(hours < 1 || hours > room.maxRentTime)
                //            errors.hours = 'The rent hours requested exceed the maximum time allowed';
                //    }
                //
                //    if(Object.keys(errors).length == 0)
                //    {
                //        let transaction = {
                //            amount: {
                //                total: parseFloat(hours * room.pricePerHour),
                //                currency: 'EUR'
                //            },
                //            description: 'Room location'
                //        };
                //
                //        switch(req.body.method)
                //        {
                //            case 'paypal':
                //                let  env = process.env.NODE_ENV == 'production'
                //                    ? cfg.hostname
                //                    : `http://${cfg.domain.dev}:${cfg.port}`;
                //
                //                return paypal.createPayment(req,
                //                    {
                //                        type         : 'paypal',
                //                        transactions : [transaction],
                //                        return_url   : env + '/orders/execute',
                //                        cancel_url   : env + '/orders/cancel'
                //                    })
                //                .then(order =>
                //                {
                //                    return Orders.trigger(req, order, 'paypal', hours, room, transaction).then( _=> {
                //                        app.success(res, {
                //                            order_url: order.url
                //                        });
                //                    });
                //                })
                //                .catch(error => console.log(error));
                //                break;
                //
                //            case 'credit_card':
                //                if(!Orders.isCardValid(req.body.card))
                //                    app.error(res, {errors: {card_number: 'The card number is invalid.'}});
                //
                //                // paypal.createPayment({
                //                //     type: 'credit_card',
                //                //     transactions: [transaction],
                //                //     credit_card: {
                //                //         number: req.body.card.number,
                //                //
                //                //     }
                //                // })
                //                // .then(payment => {
                //                //     console.log('credit_card payment: ', payment);
                //                //     app.success(res, 'ok');
                //                // })
                //                // .catch(error => {
                //                //     console.log(error)
                //                // });
                //                break;
                //        }
                //    }
                //    else
                //        app.error(res, {errors: errors});
                //})
                //.catch(error => {
                //    console.log(error)
                //    if(error) {
                //        app.error(res, {errors: {unavailable: 'This room is not available.'}})
                //    }
                //});
            }
            else {
                app.error(res, {message: 'Server error'});
            }
        },
        cancel: (req, res) => {
            return app.api.models.orders.find({
                where: {token: req.query.token},
            })
            .then(order =>
            {
                if(order) {
                    if(order.status == 'waiting') {
                        order.status = 'canceled';
                        order.save();

                        let query = qs.stringify({
                            type: 'location',
                            id: order.id
                        });

                        res.redirect('/#/orders/cancel?' + query)
                    }
                }
                else
                    app.error(res, {message: 'No order found.'});
            })
        },
        execute: (req, res) => {
            if(typeof req.query.orderId != 'undefined'
            && typeof req.query.PayerID != 'undefined'
            && typeof req.query.token != 'undefined'
            && typeof req.session.orderId != 'undefined')
            {
                if(req.query.orderId == req.session.orderId)
                {
                     return app.api.models.orders.find({
                        where: [
                            {orderId: req.query.orderId},
                            {token: req.query.token}
                        ]
                     })
                     .then(order =>
                     {
                        if(order) {
                            return paypal.execute(req.session.orderId, req.query.PayerID)
                            .then(executed => {
                                if(executed.state == 'approved' && order.status == 'waiting') {
                                    order.status = 'approved';
                                    order.payerId = req.query.PayerID;
                                    order.save();

                                    let now = new Date();
                                    let query = qs.stringify({
                                        type: 'location',
                                        id: order.id
                                    });

                                    res.redirect('/#/orders/success?' + query)
                                }
                                else {
                                    app.error(res, {message: 'Order failed'});
                                }
                            })
                        }
                     })
                }
            }
        }
    };

    return Orders;
};
