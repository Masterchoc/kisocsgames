/**
 * Created by Mel on 04/08/2016.
 */
module.exports = function(db, Sequelize, app)
{
    const Order = db.define('order',
    {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },
        status: {
            type: Sequelize.STRING,
            defaultValue: 'waiting',
        },
        token: {
            type: Sequelize.STRING,
            allowNull: false
        },
        method: {
            type: Sequelize.STRING,
            allowNull: false
        },
        amount: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        currency: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING
        },
        payerId: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        paymentId: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    
    Order.associate = function(models) {
        Order.belongsTo(models.products);
        Order.belongsTo(models.users);
    };

    return Order;
};

