
module.exports = function(app)
{
    var endpoint = '/orders';
    var ctrl = app.api.controllers.orders;

    app.get(endpoint,              ctrl.index);
    app.post(endpoint,             ctrl.create);
    app.get(endpoint+'/cancel',    ctrl.cancel);
    app.get(endpoint+'/execute',   ctrl.execute);
    app.get(endpoint+'/history',   ctrl.history);
    app.get(endpoint+'/:id',       ctrl.view);
};
