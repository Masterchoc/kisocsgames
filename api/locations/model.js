module.exports = function(db, Sequelize, app) {
    const Location = db.define('location',
        {
            id       : {
                type         : Sequelize.UUID,
                defaultValue : Sequelize.UUIDV1,
                primaryKey   : true
            },
            status   : {
                type         : Sequelize.STRING,
                defaultValue : 'order_waiting'
            },
            duration : {
                type : Sequelize.INTEGER
            },
            start    : {
                type      : Sequelize.DATE,
                allowNull : true
            },
            end      : {
                type      : Sequelize.DATE,
                allowNull : true
            }
        });
    
    Location.associate = function(models) {
        Location.belongsTo(models.products);
        Location.belongsTo(models.users);
    };
    
    return Location;
};