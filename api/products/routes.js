
module.exports = function(app)
{
    var endpoint = '/products';
    var ctrl = app.api.controllers.products;
    app.get(endpoint,           ctrl.index);
    app.get(endpoint+'/:id',    ctrl.view);
    app.put(endpoint+'/:id',    ctrl.update);
    app.delete(endpoint+'/:id', ctrl.delete);
    app.post(endpoint,          ctrl.create);
    app.get(endpoint+'-all',    ctrl.indexAll);
};
