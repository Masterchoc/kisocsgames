
module.exports = function(db, Sequelize)
{
    const Product = db.define('product',
    {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        slot_price: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        interval: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        interval_min: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        interval_max: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        image: {
            type: Sequelize.STRING,
            allowNull: true
        },
        cover: {
            type: Sequelize.STRING,
            allowNull: true
        },
        background: {
            type: Sequelize.STRING,
            allowNull: true
        },
        active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    });
    
    Product.associate = function(models) {
        Product.hasMany(models.locations);
    };
    
    return Product;
};

