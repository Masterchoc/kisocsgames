module.exports = function(app, Sequelize, db)
{
    return {
        index  : (req, res) => {
            let limit = req.query.limit || 50;
            let page  = req.query.page || 1;
            
            app.api.models.products.findAndCountAll(
            {
                limit      : limit,
                offset     : (page - 1) * limit,
                where      : {active: true}
            })
            .then(data => {
                app.success(res, data.rows);
            })
            .catch(error => {
                app.error(res, error);
            })
        },
        
        indexAll: function(req, res) {
            if(typeof req.session.user !== 'undefined') {
                if(req.session.user.role === 'admin') {
                    let limit = req.query.limit || 50;
                    let page  = req.query.page || 1;
                    app.api.models.products.findAndCountAll(
                    {
                        limit      : limit,
                        offset     : (page - 1) * limit
                    })
                    .then(data => {
                        app.success(res, data.rows);
                    })
                    .catch(error => {
                        app.error(res, error);
                    })
                }
            }
        },
        
        view   : (req, res) => {
            app.api.models.products.find({
                where: {
                    id: req.params.id,
                    active: true
                }
            })
            .then(data => {
                app.success(res, data);
            })
            .catch(error => {
                app.error(res, error);
            })
        },
        
        delete : (req, res) => {
            if(typeof req.session.user !== 'undefined') {
                if(req.session.user.role === 'admin') {
                    if(typeof req.params.id !== 'undefined') {
                        app.api.models.products.destroy({where : {id : req.params.id}})
                        .then(destroyed => app.success(res, destroyed))
                        .catch(error => app.error(res, error));
                    }
                    else {
                        app.error(res, {statusCode : 409, message : 'Missing parameters data.'})
                    }
                }
                else {
                    app.error(res, {statusCode : 401, message : 'Access Unauthorized'})
                }
            }
        },
        
        update : (req, res) => {
            if(typeof req.session.user !== 'undefined') {
                if(req.session.user.role === 'admin') {
                    if(typeof req.body.id !== 'undefined') {
                        let id = req.body.id;
                        delete req.body.id;
                        
                        if(typeof req.body.createdAt !== 'undefined')
                            delete req.body.createdAt;
                        if(typeof req.body.updatedAt !== 'undefined')
                            delete req.body.updatedAt;
                        
                        app.api.models.products.update(req.body, {where: {id: id}})
                        .then(updated => {
                            if(updated[0] === 1) {
                                app.api.models.products.findById(id).then(item => {
                                    app.success(res, item);
                                }).catch(error => app.error(res, error));
                            }
                            else
                                app.error(res, {statusCode: 409, message: 'Product not updated?.'})
                        })
                        .catch(error => app.error(res, error));
                    }
                }
                else {
                    app.error(res, {statusCode : 401, message : 'Access Unauthorized'})
                }
            }
        },
        
        create : (req, res) => {
            if(typeof req.session.user !== 'undefined') {
                if(req.session.user.role === 'admin') {
                    if(typeof req.body.id !== 'undefined')
                        delete req.body.id;
                    app.api.models.products.create(req.body).then(created => {
                        if(created)
                            app.success(res, created);
                    }).catch(error => app.error(res, error));
                }
                else
                    app.error(res, {statusCode : 401, message : 'Access Unauthorized'})
            }
        }
    };
};
